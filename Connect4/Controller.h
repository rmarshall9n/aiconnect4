#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include "SDL.h"

class Controller
{
protected:
	virtual SDL_Rect GetMove() = 0;


private:
	Controller();
	~Controller();
};

#endif // _CONTROLLER_H_