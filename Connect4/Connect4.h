#pragma once
#include "SDL.h"
#include "SDL_ttf.h"

#include "PlayerController.h"

class Connect4
{
private:
	static const int MAX_DEPTH = 3;

public:
	Connect4(void);
	~Connect4(void);

	void Initialize(int gridWidth, int gridHeight, int gridSize);
	void Update();
	bool HandleHumanInput(SDL_Event event);
	bool HandleAIInput();
	bool GameUserInput(SDL_Event event);
	int* EvaluateBoard(int player, int** grid);
	int MiniMax(int depth, bool maximizingPlayer, int** gridState);
	void Render(SDL_Surface* screen);

	bool IsExiting(){ return mIsExiting; }

private:
	TTF_Font *mFont;
	char* mMessage;

	bool mIsExiting;
	int mWinner;

	int mGridWidth;
	int mGridHeight;
	int mGridSize;

	int mPlayersTurn;
	SDL_Color mPlayer1Colour;
	SDL_Color mPlayer2Colour;
	bool mPlayer1IsAI;
	bool mPlayer2IsAI;

	int** mGrid;
	int* mCurrentEvaluation;

	bool PlaceChip(int x, int y, int** grid, int player);
	bool RemoveTopChip(int gridXPos, int** grid);

	void DrawGrid(SDL_Surface* screen);	
	void DrawChips(SDL_Surface* screen);
	bool CheckForWinner();

	void ResetGame();
};

