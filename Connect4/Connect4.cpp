#include "Connect4.h"
#include <stdlib.h>
#include <time.h>
#include <string>
#include <list>

Connect4::Connect4(void) : mIsExiting(false), mPlayersTurn(1), mMessage(""), mFont(0), mWinner(0), mGrid(0), mCurrentEvaluation(0)
{
	mPlayer1Colour.r = 255;
	mPlayer1Colour.g = 20;
	mPlayer1Colour.b = 20;

	mPlayer2Colour.r = 255;
	mPlayer2Colour.g = 255;
	mPlayer2Colour.b = 20;

	mPlayer1IsAI = false;
	mPlayer2IsAI = true;

	srand(time(NULL));
}


Connect4::~Connect4(void)
{
	delete mGrid;
	delete mCurrentEvaluation;
	// delete mFont;
}

void Connect4::Initialize(int gridWidth, int gridHeight, int gridSize)
{
	mGridWidth = gridWidth;
	mGridHeight = gridHeight;
	mGridSize = gridSize;

	// load font
	mFont = TTF_OpenFont("ARIAL.ttf", 24);

	// create grid
	mGrid = new int*[mGridHeight];
	for (int y = 0; y < mGridHeight; ++y)
		mGrid[y] = new int[mGridWidth];

	mCurrentEvaluation = new int[mGridWidth];

	ResetGame();
}

void Connect4::Update()
{
	// check user input
	SDL_Event event;
	SDL_PollEvent(&event);

	// check if the user reset the game or is trying to exit.
	if(!GameUserInput(event))
		return;

	// if theres a winner, return
	if(mWinner)
		return;

	bool turnTaken = false;

	// If player us Human
	if((mPlayersTurn == 1 && !mPlayer1IsAI) || (mPlayersTurn == 2 && !mPlayer2IsAI))
		turnTaken = HandleHumanInput(event);

	// If player is AI
	if((mPlayersTurn == 1 && mPlayer1IsAI) || (mPlayersTurn == 2 && mPlayer2IsAI))
		turnTaken = HandleAIInput();

	// If a turn has been taken, check for a winner and change the player
	if(turnTaken)
	{
		if(CheckForWinner())
		{
			mWinner = mPlayersTurn;
			mMessage = (mPlayersTurn == 1)? "Winner - Player 1" : "Winner - Player 2";
			return;
		}

		// change player
		mPlayersTurn = (mPlayersTurn == 1)? 2 : 1;
		mMessage = (mPlayersTurn == 1)? "Player 1's Turn" : "Player 2's Turn";

		mCurrentEvaluation = EvaluateBoard(mPlayersTurn, mGrid);
	}
}

bool Connect4::GameUserInput(SDL_Event event)
{
    if(event.type == SDL_KEYUP)
    {
        switch( event.key.keysym.sym )
        {
			case SDLK_ESCAPE: mIsExiting = true; return false;
			case SDLK_r: ResetGame(); return false;
            default: return true;
        }
    }
    if( event.type == SDL_QUIT )
    {
        //Quit the program
        mIsExiting = true;
		return false;
    }
    
	return true;
}

bool Connect4::HandleHumanInput(SDL_Event event)
{
	if(event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
	{
		int xPos = event.button.x - (event.button.x % mGridSize);
		int yPos = event.button.y - (event.button.y % mGridSize);

		int gridXPos = xPos / mGridSize;
		int gridYPos = yPos / mGridSize;

		//Get the mouse offsets
		if(PlaceChip(gridXPos, gridYPos, mGrid, mPlayersTurn))
			return true;
    }

	return false;
}

bool Connect4::HandleAIInput()
{
	/*
	mCurrentEvaluation = EvaluateBoard(mPlayersTurn, mGrid);
	int topScore = 0;
	int numTopScores = -1;

	int mTopColls[7];

	// find top score
	for(int i = 0; i < mGridWidth; i++)
	{
		if(mCurrentEvaluation[i] > topScore)
			topScore = mCurrentEvaluation[i];
	}

	// find duplicate top scores
	for(int i = 0; i < mGridWidth; i++)
	{
		if(mCurrentEvaluation[i] == topScore)
		{
			numTopScores++;
			mTopColls[numTopScores] = i;
		}
	}
	
	// if one top score place
	int bestCol = 0;

	if(numTopScores == 0)
		bestCol == 0;
	else
		bestCol = rand() % numTopScores;

	// place the chip
	if(PlaceChip(mTopColls[bestCol], 0, mGrid, mPlayersTurn))
		return true;
*/

	// create a copy of the grid
	int** tempGrid = new int*[mGridHeight];
	for (int y = 0; y < mGridHeight; ++y)
	{
		tempGrid[y] = new int[mGridWidth];
		for(int x = 0; x <  mGridWidth; x++)
		{
			tempGrid[y][x] = mGrid[y][x];
		}
	}



	int bestCol = MiniMax(0, true, tempGrid);
	if(PlaceChip(bestCol, 0, mGrid, mPlayersTurn))
		return true;

	return false;
}

int* Connect4::EvaluateBoard(int player, int** grid)
{
	if(!(player == 1 || player == 2))
		return 0;

	int bestCol = 0;
	int highestScore = 0;

	int* evaluation = new int[mGridWidth];

	for(int x = 0; x < mGridWidth; x++)
	{
		for(int y = mGridHeight-1; y > 0; y--)
		{
			// lowest free space in grid
			if(grid[y][x] == 0)
			{
				int moveScore = 0;
				int p1count = 0;
				int p2count = 0;

				// EVALUATE VERTICAL LINE
				// ====================================================================================================
				for(int group = 0; group < 4; group++)
				{
					p1count = 0;
					p2count = 0;
					
					// for each cell in the line
					for(int cell = 0; cell < 4; cell++)
					{
						int cellToCheck = y + cell;

						if(cellToCheck >= 0 && cellToCheck < mGridHeight)
						{
							//if(grid[cellToCheck][x] == 0)
							//	blankcount++;

							if(grid[cellToCheck][x] == 1)
								p1count++;

							else if(grid[cellToCheck][x] == 2)
								p2count++;
						}
						else
							break;
					}

					// If none of the oposition, score the move depending on how many moves in group.
					if(player == 1 && p2count == 0)
						moveScore += p1count;

					else if(player == 2 && p1count == 0)
						moveScore += p2count;

					// check if opponent is about to win
					if(player == 1 && p1count == 0 && p2count == 3)
						moveScore += 1000;
					else if(player == 2 && p2count == 0 && p1count == 3)
						moveScore += 1000;
				}

				




				// EVALUATE HORIZONTAL LINE
				// ====================================================================================================
				// There are 4 sets of 4 which relate to the current cell
				// e.g. |1 1 1 1
				//      |  2 2 2 2
				//      |    3 3 3 3
				//      |      4 4 4 4
				//      |0 0 0 x 0 0 0
 				//		==================
				for(int group = 0; group < 4; group++)
				{
					p1count = 0;
					p2count = 0;

					// check each cell in the current set
					for(int cell = 0; cell < 4; cell++)
					{
						int cellToCheck = x - group + cell;

						if(cellToCheck >= 0 && cellToCheck < mGridWidth)
						{
							//if(grid[y][cellToCheck] == 0)
							//	blankcount++;

							if(grid[y][cellToCheck] == 1)
								p1count++;

							else if(grid[y][cellToCheck] == 2)
								p2count++;
						}						
					}

					// If none of the oposition, score the move depending on how many moves in group.
					if(player == 1 && p2count == 0)
						moveScore += p1count;

					else if(player == 2 && p1count == 0)
						moveScore += p2count;

					// check if opponent is about to win
					if(player == 1 && p1count == 0 && p2count == 3)
						moveScore += 1000;
					else if(player == 2 && p2count == 0 && p1count == 3)
						moveScore += 1000;
				}



				// EVALUATE DIAGANOL DOWN RIGHT
				// ====================================================================================================
				// only evaluate if we can get a possibility of 4 in a row
				// bottom left can only get 1, 2 or 3
				if( !(x < 3 && y == 5) ||
					!(x < 2 && y == 4) ||
					!(x < 1 && y == 3))
				{
					// for each set of possible lines of 4
					for(int group = 0; group < 4; group++)
					{
						p1count = 0;
						p2count = 0;

						// get count in group
						for(int cell = 0; cell < 4; cell++)
						{
							int yCellToCheck = y - group + cell;
							int xCellToCheck = x - group + cell;

							if(	yCellToCheck >= 0 && yCellToCheck < mGridHeight &&
								xCellToCheck >= 0 && xCellToCheck < mGridWidth)
							{
								//if(grid[yCellToCheck][xCellToCheck] == 0)
								//	blankcount++;

								if(grid[yCellToCheck][xCellToCheck] == 1)
									p1count++;

								else if(grid[yCellToCheck][xCellToCheck] == 2)
									p2count++;
							}
							else
								break;
						}

						// If none of the oposition, score the move depending on how many moves in group.
						if(player == 1 && p2count == 0)
							moveScore += p1count;

						else if(player == 2 && p1count == 0)
							moveScore += p2count;

						// check if opponent is about to win
						if(player == 1 && p1count == 0 && p2count == 3)
							moveScore += 1000;
						else if(player == 2 && p2count == 0 && p1count == 3)
							moveScore += 1000;
					}
				}


				// EVALUATE DIAGANOL DOWN LEFT
				// ====================================================================================================
				// only evaluate if we can get a possibility of 4 in a row
				// bottom left can only get 1, 2 or 3
				if( !(x > 3 && y == 5) ||
					!(x > 4 && y == 4) ||
					!(x > 5 && y == 3))
				{
					// for each set of possible lines of 4
					for(int group = 0; group < 4; group++)
					{
						p1count = 0;
						p2count = 0;

						// get count in group
						for(int cell = 0; cell < 4; cell++)
						{
							int yCellToCheck = y - group + cell;
							int xCellToCheck = x + group - cell;

							if(	yCellToCheck >= 0 && yCellToCheck < mGridHeight &&
								xCellToCheck >= 0 && xCellToCheck < mGridWidth)
							{
								//if(grid[yCellToCheck][xCellToCheck] == 0)
								//	blankcount++;

								if(grid[yCellToCheck][xCellToCheck] == 1)
									p1count++;

								else if(grid[yCellToCheck][xCellToCheck] == 2)
									p2count++;
							}
							else
								break;
						}

						// If none of the oposition, score the move depending on how many moves in group.
						if(player == 1 && p2count == 0)
							moveScore += p1count;

						else if(player == 2 && p1count == 0)
							moveScore += p2count;

						// check if opponent is about to win
						if(player == 1 && p1count == 0 && p2count == 3)
							moveScore += 1000;
						else if(player == 2 && p2count == 0 && p1count == 3)
							moveScore += 1000;
					}
				}
				// ====================================================================================================

				if(moveScore > highestScore)
					bestCol = x;
				
				evaluation[x] = moveScore;
				break;
			}
		}

	}

	return evaluation;
}

int Connect4::MiniMax(int depth, bool maximizingPlayer, int** gridState)
{

	if(depth == MAX_DEPTH || CheckForWinner())
	{
		// decide which player to evaluate
		int player = ((mPlayersTurn == 1 && maximizingPlayer) || (mPlayersTurn == 2 && !maximizingPlayer))? 1 : 2;

		int* evaluation = new int[mGridWidth];
		evaluation = EvaluateBoard(player, gridState);

		// get the score for the node
		int nodeScore = 0;
		for(int i = 0; i < mGridWidth; i++)
			nodeScore += evaluation[i];

		return nodeScore;
	}
	
	int minimaxValue = -1;
	int bestMove = 0;
	// loop over best moves
	for(int i = 0; i < mGridWidth; i++)
	{
		// update the grid
		if(PlaceChip(i, 0, gridState, 1))
		{
			// step into next move
			int returnScore = MiniMax(depth+1, !maximizingPlayer, gridState);
			
			// reset the board state
			RemoveTopChip(i, gridState);


			// check minimax value
			if(minimaxValue == -1)
				minimaxValue = returnScore;

			// if maximising this turn then we want the min value from the next branch down. vice versa
			else if ((maximizingPlayer && returnScore < minimaxValue) || (!maximizingPlayer && returnScore > minimaxValue))
			{
				minimaxValue = returnScore;

				if(!depth)
					bestMove = i;
			}
		}
	}

	if(!depth)
		return bestMove;
	
	return minimaxValue;
}

// old minimax
/*
void Connect4::MiniMax(int depth, bool maximizingPlayer, int** gridState)
{
	if(depth == MAX_DEPTH)
		return;

	// evaluate board
	//int* evaluation = EvaluateBoard(1, gridState);
	
	// check for best move
	int bestScore = 0;
	for(int i = 0; i < mGridWidth; i++)
		bestScore = (evaluation[i] > bestScore)? evaluation[i] : bestScore;

	// loop over best moves
	for(int i = 0; i < mGridWidth; i++)
	{
		if(evaluation[i] == bestScore) // if best score
		{
			// update the grid
			if(PlaceChip(i, 0, gridState, 1))
			{
				// step into next move
				MiniMax(depth+1, !maximizingPlayer, gridState);
				
				// reset the board state
				RemoveTopChip(i, gridState);
			}
		}
	}

	// evaluate best path
	// for(int i = 0; i < moves.length; i++) {

	//}

	// return bestPath;
}

*/


bool Connect4::PlaceChip(int gridXPos, int gridYPos, int** grid, int player)
{
	if(grid[gridYPos][gridXPos] == 0)
	{
		int lowestFreeY = gridYPos;
		for(int i = gridYPos + 1; i < mGridHeight; i++)
		{
			if(grid[i][gridXPos] != 0)
				break;

			lowestFreeY = i;
		}

		grid[lowestFreeY][gridXPos] = player;
		return true;
	}

	return false;
}

bool Connect4::RemoveTopChip(int gridXPos, int** grid)
{
	// cycle down a column
	for(int y = 0; y < mGridHeight -1; y++)
	{
		// if we find a chip set it to 0 and return true
		if(grid[y][gridXPos] != 0)
			grid[y][gridXPos] = 0;
			return true;
	}

	// no chip removed (col empty)
	return false;
}

bool Connect4::CheckForWinner()
{
	for (int y = 0; y < mGridHeight; ++y)
	{
		for (int x = 0; x < mGridWidth; ++x)
		{
			int playerInCell = mGrid[y][x];

			if(playerInCell != 0)
			{

				// check row
				if(x < mGridWidth - 3)
				{
					if(mGrid[y][x+1] == playerInCell && mGrid[y][x+2] == playerInCell && mGrid[y][x+3] == playerInCell)
					{
						return true;
					}
				}
				
				// check col
				if(y < mGridHeight - 3)
				{
					if(mGrid[y+1][x] == playerInCell && mGrid[y+2][x] == playerInCell && mGrid[y+3][x] == playerInCell)
					{
						return true;
					}
				}

				// down diag
				if(x < mGridWidth - 3 && y < mGridHeight - 3)
				{
					if(mGrid[y+1][x+1] == playerInCell && mGrid[y+2][x+2] == playerInCell && mGrid[y+3][x+3] == playerInCell)
					{
						return true;
					}
				}
				
				// up diag
				if(x < mGridWidth - 3 && y > 3)
				{
					if(mGrid[y-1][x+1] == playerInCell && mGrid[y-2][x+2] == playerInCell && mGrid[y-3][x+3] == playerInCell)
					{
						return true;
					}
				}
				
			}
		}
	}

	return false;
}

void Connect4::ResetGame()
{
	// clear each cell to 0
	for (int y = 0; y < mGridHeight; ++y)
	{
		for (int x = 0; x < mGridWidth; ++x)
			mGrid[y][x] = 0;
	}

	for (int i = 0; i < mGridWidth; i++)
		mCurrentEvaluation[i] = 0;

	mWinner = 0;
	mPlayersTurn = 1;
	mMessage = "Player 1's Turn";
}

void Connect4::Render(SDL_Surface* screen)
{
	DrawChips(screen);
	DrawGrid(screen);


	// render weighting
	for(int x = 0; x < mGridWidth; x++)
	{
		
		SDL_Rect offset = {(x * mGridSize) + mGridSize / 2, mGridHeight * mGridSize + 10};
		SDL_Color colour = (mPlayersTurn == 1)? mPlayer1Colour : mPlayer2Colour;

		SDL_Surface *textSurface;
		std::string weightingString = std::to_string(mCurrentEvaluation[x]);
		if((textSurface=TTF_RenderText_Solid(mFont,weightingString.c_str(),colour))) {

			SDL_BlitSurface(textSurface,NULL,screen,&offset);
			SDL_FreeSurface(textSurface);
		}
	}

	// render message
	SDL_Rect offset = {(mGridWidth * mGridSize) / 2 - 100, mGridWidth * mGridSize + 10};
	SDL_Color colour = (mPlayersTurn == 1)? mPlayer1Colour : mPlayer2Colour;

    SDL_Surface *textSurface;
	if((textSurface=TTF_RenderText_Solid(mFont,mMessage,colour))) {

        SDL_BlitSurface(textSurface,NULL,screen,&offset);
        SDL_FreeSurface(textSurface);
    }
}

void Connect4::DrawGrid(SDL_Surface* screen)
{
	// draw Grid
	for(int x = 1; x < mGridWidth; x++)
	{
		SDL_Rect line;
		line.h = mGridSize * mGridHeight;
		line.w = 2;
		line.x = mGridSize * x;
		line.y = 0;
			
		SDL_FillRect(screen, &line, 200);
	}

	for(int y = 1; y < mGridHeight + 1; y++)
	{
		SDL_Rect line;
		line.h = 2;
		line.w = mGridSize * mGridWidth;
		line.x = 0;
		line.y = mGridSize * y;
			
		SDL_FillRect(screen, &line, 200);
	}
}

void Connect4::DrawChips(SDL_Surface* screen)
{
	for (int y = 0; y < mGridHeight; ++y)
	{
		for (int x = 0; x < mGridWidth; ++x)
		{
			if(mGrid[y][x] != 0)
			{
				SDL_Rect line;
				line.h = mGridSize;
				line.w = mGridSize;
				line.x = mGridSize * x;
				line.y = mGridSize * y;

				if(mGrid[y][x] == 1)
					SDL_FillRect(screen, &line, SDL_MapRGB(screen->format , 255 , 20 , 20));
				if(mGrid[y][x] == 2)
					SDL_FillRect(screen, &line, SDL_MapRGB(screen->format , 255 , 255 , 20));
			}
		}
	}
}