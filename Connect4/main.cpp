#include "SDL.h"
#include "Connect4.h"

int main( int argc, char* args[] )
{
	SDL_Surface* screen = NULL;
    
	//Start SDL
    SDL_Init( SDL_INIT_EVERYTHING );

    //Initialize SDL_ttf
    if( TTF_Init() == -1 )
    {
        return false;    
    }

	const int gridSize = 100;
	const int gridWidth = 7;
	const int gridHeight = 6;
	const int messageHeight = 200;

	//Set up screen
	screen = SDL_SetVideoMode( gridSize * gridWidth, gridSize * gridHeight + messageHeight, 32, SDL_SWSURFACE );
	SDL_WM_SetCaption( "Connect 4 - AI", NULL);

	Connect4 game;
	game.Initialize(gridWidth, gridHeight, gridSize);

	while (!game.IsExiting())
	{
		// Update
		game.Update();

		// Render
		SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format,0,0,0));
		game.Render(screen);

		SDL_Flip( screen );
	}

    //Quit SDL
    SDL_Quit();
    
    return 0;    
}