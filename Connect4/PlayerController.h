#ifndef _PLAYER_CONTROLLER_H_
#define _PLAYER_CONTROLLER_H_

#include "Controller.h"
#include "SDL.h"

class PlayerController : public Controller
{
public:
	PlayerController();
	~PlayerController();


	SDL_Rect GetMove();


};

#endif // _PLAYER_CONTROLLER_H_